
var stopRender = false;
var camera, scene, renderer;
var geometry, material, mSol;
var mouse;
var ang1 = 0.0;
var ang2 = 0.0;
var ang3 = 0.0;
var ang4 = 0.0;
var ang5 = 0.0;
var ang6 = 0.0;
var ang7 = 0.0;
var ang8 = 0.0;
//crear orbita
init();
animate();
function AgregOrbita(scene, radio) {
    var curve = new THREE.EllipseCurve(0,  0, radio, radio, 0,  6.28, false, 0);
    var points = curve.getPoints( 50 );
    var geometry = new THREE.BufferGeometry().setFromPoints( points );
    var material = new THREE.LineBasicMaterial( { color : 0xffffff } );
    var ellipse = new THREE.Line( geometry, material );
        ellipse.rotation.x = 3.14/2;
        scene.add( ellipse );
}
    
function init() {
    
// AGRANDAR EL SISTEMA ( MANEJANDO "d")
    scene = new THREE.Scene();
    var aspect = window.innerWidth / window.innerHeight;
    var d = 1.4;
    camera = new THREE.OrthographicCamera( - d * aspect, d * aspect, d, - d, 1, 1000 ); 

    camera.position.set( 20, 20, 20 ); 
    camera.lookAt( scene.position );  
    //fondo negro

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setSize( window.innerWidth, window.innerHeight);
    document.body.appendChild( renderer.domElement );
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    
    //Sol
    
    geometry = new THREE.SphereGeometry( 0.1, 32, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0xFFD700} );
    mSol = new THREE.Mesh( geometry, material );
    scene.add( mSol );
    
    //Mercurio
    
    geometry = new THREE.SphereGeometry( 0.03, 32, 32 );
    material = new THREE.MeshBasicMaterial( {color: 0x906002} );
    mPlan1 = new THREE.Mesh( geometry, material );
    scene.add( mPlan1 );
    AgregOrbita(scene, 0.2); //Órbita

    //Venus

geometry = new THREE.SphereGeometry( 0.04, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0xFFA500} );
mPlan2 = new THREE.Mesh( geometry, material );
scene.add( mPlan2 );
AgregOrbita(scene, 0.4); //Órbita

//Tierra

geometry = new THREE.SphereGeometry( 0.05, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0x4169E1} );
mPlan3 = new THREE.Mesh( geometry, material );
scene.add( mPlan3 );
AgregOrbita(scene, 0.6); //Órbita

//Marte

geometry = new THREE.SphereGeometry( 0.04, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0xFF4500} );
mPlan4 = new THREE.Mesh( geometry, material );
scene.add( mPlan4 );
AgregOrbita(scene, 0.8); //Órbita

//Jupiter

geometry = new THREE.SphereGeometry( 0.08, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0x9C953B} );
mPlan5 = new THREE.Mesh( geometry, material );
scene.add( mPlan5 );
AgregOrbita(scene, 1); //Órbita

    //Saturno

geometry = new THREE.SphereGeometry( 0.06, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0xC3B608} );
mPlan6 = new THREE.Mesh( geometry, material );
scene.add( mPlan6 );
AgregOrbita(scene, 1.2); //Órbita

 //Urano

geometry = new THREE.SphereGeometry( 0.07, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0x39C3D1} );
mPlan7 = new THREE.Mesh( geometry, material );
scene.add( mPlan7 );
AgregOrbita(scene, 1.4); //Órbita

//Neptuno

geometry = new THREE.SphereGeometry( 0.065, 32, 32 );
material = new THREE.MeshBasicMaterial( {color: 0x145CD9} );
mPlan8 = new THREE.Mesh( geometry, material );
scene.add( mPlan8 );
AgregOrbita(scene, 1.6); //Órbita


    //Objetos adicionales
    mouse = new THREE.Vector2();
}

function animate() {
    requestAnimationFrame( animate );
    if (stopRender) return;
//definimos velocidad
    ang1 = ang1 - 0.03;
    ang2 = ang2 - 0.02;
    ang3 = ang3 - 0.013;
    ang4 = ang4 - 0.01;
    ang5 = ang5 - 0.007;
    ang6 = ang6 - 0.009;
    ang7 = ang7 - 0.008;
    ang8 = ang8 - 0.0075;
    //Rotación del sol
    mSol.rotation.y += 0.03;
    //Traslación de planeta(define el angulo de giro)
    mPlan1.position.x = 0.2 * Math.cos(ang1);
    mPlan1.position.z = 0.2 * Math.sin(ang1);
    
    mPlan2.position.x = 0.4 * Math.cos(ang2);
    mPlan2.position.z = 0.4 * Math.sin(ang2);

    mPlan3.position.x = 0.6 * Math.cos(ang3);
    mPlan3.position.z = 0.6 * Math.sin(ang3);
    mPlan3.rotation.y += 0.1;
    
    mPlan4.position.x = 0.8 * Math.sin(-ang4);
    mPlan4.position.z = 0.8 * Math.cos(ang4);
    mPlan4.rotation.y += 0.08;

    mPlan5.position.x = 1 * Math.sin(-ang5);
    mPlan5.position.z = 1 * Math.cos(ang5);

    mPlan6.position.x = 1.2 * Math.sin(-ang6);
    mPlan6.position.z = 1.2 * Math.cos(ang6);
    
    mPlan7.position.x = 1.4 * Math.sin(-ang7);
    mPlan7.position.z = 1.4 * Math.cos(ang7);

    mPlan8.position.x = 1.6 * Math.sin(-ang8);
    mPlan8.position.z = 1.6 * Math.cos(ang8);



//    camera.rotation.z += 0.005;
    renderer.render( scene, camera );
}

function onDocumentMouseDown( event ) {
    event.preventDefault();
    stopRender = !stopRender;
}